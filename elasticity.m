classdef elasticity

% elasticity class stores properties of the elastic solid
% bounding a fault, as well as loading applied to that fault

    properties
    
      L % length of fault
      x % grid points along fault
      h % grid spacing
      k % wavenumber

      mu % shear modulus
      c % shear wave speed
      rho % density

      eta % radiation-damping coefficient
      K % spring-slider stiffness
      
      % parameters for loading
      Neff % effective normal stress
      T0 % initial shear stress
      W % width of stress perturbation
      A % amplitude of stress perturbation
      dTdt % stressing rate
      
    end

    methods
    
    
    function El = elasticity(N,L,mu,c,Neff,T0,A,W,dTdt)
      % class constructor

      El.L = L;
      El.mu = mu;
      El.c = c;
      El.rho = El.mu/El.c^2;
      El.eta = 0.5*El.mu/El.c;
      El = El.discretize(N);
      
      El.Neff = Neff;
      El.T0 = T0;
      El.A = A;
      El.W = W;
      El.dTdt = dTdt;
      
    end

    
    function El = discretize(El,N)
      % discretize fault and initialize stiffness or space/wavenumber arrays

      if N==1
	% spring-slider model (K=stiffness)
	El.K = (2/pi)*El.mu/El.L;
	El.x = 0;
      else
	% finite fault (h=grid spacing, x=fault coordinates)
	El.h = El.L/N;
	El.x = [0:N-1]'*El.h-El.L/2;
	% k=wavenumber array (for FFT)
	El.k = zeros(N,1);
	El.k(1:N/2+1) = 2*[0:N/2]/N;
	El.k(N:-1:N/2+2) = -El.k(2:N/2);
	El.k = El.k*pi/El.h;
      end
    
    end
    
    
    function x = getGrid(El)
      x = El.x;
    end
    
    
    function dt = timeStep(El)
      % approximate time step
      dt = 0.5*El.h/El.c;
    end
    
    
    function deltaT = slip2stress(El,D)
      % slip2stress takes slip D on uniformly spaced grid
      % and calculates stress change deltaT on that grid,
      % using Fourier transform method (periodic BC along fault)
      
      N = length(El.x); % number of grid points
      
      if N==1
	% spring-slider
	deltaT = -El.K*D;
      else
	% finite fault
	Dhat = fft(D);
	That = -0.5*El.mu*abs(El.k).*Dhat;
	deltaT = ifft(That);
      end
    
    end
   
    
    function [T0,Neff] = loadStress(El,t)
      % evaluates at time t on fault grid:
      % T0 = shear stress in absence of slip
      % Neff = effective normal stress
      
      N = length(El.x);
      
      % constant background (increasing linearly in time)
      % + Gaussian perturbation
      
      T0 = El.T0; % base level for stress
      T0 = T0 + El.A*exp(-0.5*(El.x/El.W).^2); % add perturbation
      T0 = T0 + El.dTdt*t; % increase overall pattern at constant rate
      
      % effective normal stress
      Neff = repmat(El.Neff,[N,1]); % spatially uniform
      
    end
    
    
end
    
end