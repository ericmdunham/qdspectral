% script to run QDspectral for spring-slider with rate-and-state friction

N = 1; % one grid point for spring-slider
L = 1; % fault length (m), used in setting spring stiffness
mu = 30e3; % shear modulus (MPa)
c = 3e3; % shear wave speed (m/s)

frictionLaw = 'ratestate'; % friction law
frictionParams.evolutionLaw = 'aging'; % 'aging' or 'slip' evolution law
frictionParams.f0 = 0.6; % reference friction (-)
frictionParams.V0 = 1e-6; % reference velocity (m/s)
frictionParams.dc = 10e-6; % state evolution distance (m)
frictionParams.a = 0.01; % direct effect parameter (-)
frictionParams.b = 0.014; % evolution parameter (-)

Neff = 100; % effective normal stress (MPa)
T0 = frictionParams.f0*Neff; % initial shear stress (MPa)
A = 0; % amplitude of stress perturbation (MPa)
W = Inf; % width of stress perturbation (m)
dTdt = 1e1; % shear stressing rate (MPa/s)

timeStepMethod = 'forwardEuler'; % time-stepping method

% initialize solver class (which will initialize elasticity and friction classes)
Sol = solver(N,L,mu,c,frictionLaw,frictionParams,Neff,T0,A,W,dTdt,timeStepMethod);

% set initial conditions
t = 0; % initial time
D = 0; % initial slip
Psi = frictionParams.f0;
Sol = Sol.initialConditions(t,D,Psi);

% time step
tmax = 1e0; % total time (s)
nt = 1000; % number of time steps
dt = tmax/nt; % time step (s)

t = nan(1,nt+1); % time (s)
D = nan(1,nt+1); % slip (m)
V = nan(1,nt+1); % slip velocity (m/s)
T = nan(1,nt+1); % shear stress (MPa)

[t(1),D(1),V(1),T(1)] = Sol.getSolution(); % get initial conditions

% time step loop
for n=1:nt
  Sol = Sol.timeStep(dt); % advance solution by dt
  [t(n+1),D(n+1),V(n+1),T(n+1)] = Sol.getSolution(); % get solution
  if mod(n,10)==0, plot(t,V), xlim([0 tmax]), drawnow, end % optional plot
end

plot(t,V)
xlim([0 tmax])
xlabel('time (s)')
ylabel('slip velocity (m/s)')