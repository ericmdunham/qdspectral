% script to run QDspectral for finite fault with rate-and-state friction
%
% central VW patch surrounded by VS

N = 256; % number of Fourier sample points along fault
L = 16; % fault length (m)
mu = 30e3; % shear modulus (MPa)
c = 3e3; % shear wave speed (m/s)

frictionLaw = 'ratestate'; % friction law
frictionParams.evolutionLaw = 'slip'; % 'aging' or 'slip' evolution law
frictionParams.f0 = 0.6; % reference friction (-)
frictionParams.V0 = 1e-6; % reference velocity (m/s)
frictionParams.dc = 10e-6; % state evolution distance (m)
frictionParams.a = 0.01; % direct effect parameter (-)
frictionParams.b = 0.014; % evolution parameter (-)
frictionParams.widthVW = 1.25; % width of central velocity-weakening region
frictionParams.aVS = 0.02; % direct effect parameter (outside VW region)
frictionParams.bVS = 0.014; % evolution parameter (outside VW region)

Neff = 100; % effective normal stress (MPa)
T0 = frictionParams.f0*Neff; % initial shear stress (MPa)
A = 0; % amplitude of stress perturbation (MPa)
W = Inf; % width of stress perturbation (m)
dTdt = 1e-6; % shear stressing rate (MPa/s)

tmax = 1e11; % total simulation time (s)
nt = 10000; % maximum number of time steps
timeStepMethod = 'forwardEuler'; % time-stepping method

% initialize solver class (which will initialize elasticity and friction classes)
Sol = solver(N,L,mu,c,frictionLaw,frictionParams,Neff,T0,A,W,dTdt,timeStepMethod);

% get x = distance along fault (m)
x = Sol.El.getGrid();

% set initial conditions
t = 0; % initial time
D = zeros(N,1); % initial slip
Psi = frictionParams.f0+zeros([N 1]);
Sol = Sol.initialConditions(t,D,Psi);

[t(1),D(:,1),V(:,1),T(:,1),Psi(:,1)] = Sol.getSolution(); % get initial conditions

% time step loop
for n=1:nt
  dt = 0.05*frictionParams.dc/max(V(:,n));% set time step to resolve state evolution
  if t(n)+dt>tmax, dt = tmax-t(n); end % stop at tmax
  Sol = Sol.timeStep(dt); % advance solution by dt
  [t(n+1),D(:,n+1),V(:,n+1),T(:,n+1),Psi(:,n+1)] = Sol.getSolution(); % get solution
  if mod(n,10)==0 % optional plot
    semilogy(x,V(:,n+1))
    xlim([-L/2 L/2])
    xlabel('distance (m)')
    ylabel('slip velocity (m/s)')
    drawnow
  end
  if t(n+1)>=tmax, break, end % stop when t exceeds tmax
  if max(V(:,n+1),[],1)>1, break, end % user-specific stopping criterion
end

nt = length(t)-1; % adjust nt to be total number of time steps taken

pcolored(x,t,log10(V)),shading flat,colorbar
xlim([-L/2 L/2])
xlabel('distance (m)')
ylabel('time (s)')
title('log slip velocity (m/s)')