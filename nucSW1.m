% script to run QDspectral for finite fault with slip-weakening friction

N = 512; % number of grid points
L = 10; % fault length (m)
mu = 30e3; % shear modulus (MPa)
c = 3e3; % shear wave speed (m/s)

frictionLaw = 'slipweak'; % friction law
frictionParams.fs = 0.8; % static friction
frictionParams.fd = 0.7; % dynamic friction
frictionParams.Dc = 10e-6; % slip-weakening distance (m)

Neff = 100; % effective normal stress (MPa)
A = 10; % amplitude of stress perturbation (MPa)
T0 = frictionParams.fs*Neff-A; % initial shear stress (MPa)
W = 1; % width of stress perturbation (m)
dTdt = 10; % shear stressing rate (MPa/s)

timeStepMethod = 'forwardEuler'; % time-stepping method

% initialize solver class (which will initialize elasticity and friction classes)
Sol = solver(N,L,mu,c,frictionLaw,frictionParams,Neff,T0,A,W,dTdt,timeStepMethod);

% get x = distance along fault (m)
x = Sol.El.getGrid();

% set initial conditions
t = 0; % initial time
D = zeros(N,1); % initial slip
Sol = Sol.initialConditions(t,D);

% time step
tmax = 2e-3; % total time (s)
dt = Sol.suggestTimeStep(); % get recommended time step
nt = ceil(tmax/dt); % number of time steps
dt = tmax/nt; % time step (s)

t = nan(1,nt+1); % time (s)
D = nan(N,nt+1); % slip (m)
V = nan(N,nt+1); % slip velocity (m/s)
T = nan(N,nt+1); % shear stress (MPa)

[t(:,1),D(:,1),V(:,1),T(:,1)] = Sol.getSolution(); % get initial conditions

% time step loop
for n=1:nt
  Sol = Sol.timeStep(dt); % advance solution by dt
  [t(n+1),D(:,n+1),V(:,n+1),T(:,n+1)] = Sol.getSolution(); % get solution
  if mod(n,10)==0, plot(x,V(:,n+1)), xlim([-L/2 L/2]), drawnow, end % optional plot
end

pcolored(x,t,log10(V)),shading flat,colorbar
xlabel('distance (m)')
ylabel('time (s)')
title('log slip velocity (m/s)')