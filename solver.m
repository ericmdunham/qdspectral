classdef solver

% solver class implements time stepping for coupled
% friction + elasticity problems using quasi-dynamic elasticity

  properties
    timeStepMethod % time step method ('forwardEuler','adaptiveRK')
    El = elasticity.empty; % elasticity class
    Fr = friction.empty; % friction class
    t % time
    D % slip
    V % slip velocity
    T % shear stress
    T0 % shear stress in absence of slip
    Neff % effective normal stress
    Psi % state variable
    DPsi % state rate
  end


  methods
    
    
  function Sol = solver(N,L,mu,c,frictionLaw,frictionParams,Neff,T0,A,W,dTdt,timeStepMethod)
    % class constructor
    Sol.El = elasticity(N,L,mu,c,Neff,T0,A,W,dTdt);
    Sol.Fr = friction(frictionLaw,frictionParams);
    Sol.timeStepMethod = timeStepMethod;
    Sol.D = nan(N,1);
    Sol.V = nan(N,1);
    Sol.T = nan(N,1);
    Sol.T0 = nan(N,1);
    Sol.Neff = nan(N,1);
    Sol.Psi = nan(N,1);
    Sol.DPsi = nan(N,1);
  end
  
  
  function Sol = initialConditions(Sol,t,D,Psi)
    % set initial conditions on slip D and state Psi at time t
    Sol.t = t;
    Sol.D = D;
    if nargin==4, Sol.Psi = Psi; end
    Sol = Sol.rates(); % calculate V,T,Dpsi associated with initial condition
  end


  function Sol = rates(Sol)
    % evaluates slip velocity V, shear stress T, and state rate DPsi=dPsi/dt 
    % at current time Sol.t
    
    % This is done by solving two equations:
    % 1. elasticity: T = T0+deltaT-eta*V (see below for definitions)
    % 2. friction: 
    %  a. (slip-weakening) Tstr = f*Neff (with T=Tstr during slip, T<Tstr when locked)
    %  b. (rate-and-state) Tstr = f(V,Psi)*Neff (with T=Tstr always)
    
    % first, calculate "load" T0 = shear stress in absence of slip
    % and Neff = effective normal stress
    % (these can be time-dependent quantities)
    [Sol.T0,Sol.Neff] = Sol.El.loadStress(Sol.t);

    % calculate (static) stress changes caused by slip
    deltaT = Sol.El.slip2stress(Sol.D);
    
    switch Sol.Fr.frictionLaw
      
      case 'slipweak'
	
	% calculate friction coefficient
	f = Sol.Fr.frictionCoeff(Sol.El.x,Sol.D);
	
	% now loop over all points on fault and set V,T
	N = length(Sol.El.x);
	
	for i=1:N
	  
	  % calculate stress acting on fault in absence of additional fault slip
	  Tlock = Sol.T0(i)+deltaT(i);
	  
	  % calculate fault strength
	  Tstr = f(i)*Sol.Neff(i);
	  
	  if Tlock<=Tstr
	    % locked
	    Sol.T(i) = Tlock;
	    Sol.V(i) = 0;
	  else
	    % slipping
	    Sol.T(i) = Tstr;
	    Sol.V(i) = -(Tstr-Tlock)/Sol.El.eta;
	  end
	  
	end
    
      case 'ratestate'
	
	% loop over all points on fault and set V,T,DPsi
	N = length(Sol.El.x);
	
	for i=1:N
	  
	  % calculate stress acting on fault in absence of additional fault slip
	  Tlock = Sol.T0(i)+deltaT(i);

	  % get friction law parameters
	  frictionParams = Sol.Fr.getParams(Sol.El.x(i));
	  
	  % solve rate-and-state friction together with elasticity
	  [Sol.V(i),Sol.T(i),Sol.DPsi(i)] = solver.newtonRS(Sol.V(i),Sol.Psi(i),Tlock,Sol.Neff(i),Sol.El.eta,frictionParams);

	end
	
      otherwise
	
	error('invalid friction law')

    end
	
  end
  
  
  function dt = suggestTimeStep(Sol)
    dt = Sol.El.timeStep();
  end
  

  function Sol = timeStep(Sol,dt)
    % advances solution by time step dt
    
    switch Sol.timeStepMethod
      
      case 'forwardEuler' % forward Euler
    
	% evaluate rates at time t
	Sol = Sol.rates();
	
	% update slip and state
	Sol.D = Sol.D+dt*Sol.V;
	Sol.Psi = Sol.Psi+dt*Sol.DPsi;
	
	% update time
	Sol.t = Sol.t+dt;
    
      case 'forwardEuler2'

	N = length(Sol.El.x);
	
	Y = [Sol.D; Sol.Psi];
	t = Sol.t;
	
	dYdt = evalODE(t,Y,Sol);

	Sol.V = dYdt(1:N);
	Sol.DPsi = dYdt(N+1:2*N);
	
	% update slip and state
	Sol.D = Sol.D+dt*Sol.V;
	Sol.Psi = Sol.Psi+dt*Sol.DPsi;
	
	% update time
	Sol.t = Sol.t+dt;
	
      case 'adaptiveRK'

	rtol = 1e-9; % relative tolerance
	atol = 1e3*eps; % absolute tolerance
	
	opt = odeset('RelTol',rtol,'AbsTol',atol);
	sol = ode45(@evalODE,[Sol.t Sol.t+dt],[Sol.D; Sol.Psi],opt,Sol);

	% update time
	Sol.t = Sol.t+dt;

	% evaluate fields
	[Y,dYdt] = deval(Sol.t,sol);

	N = length(Sol.El.x);
	Sol.D = Y(1:N);
	Sol.Psi = Y(N+1:2*N);	
	Sol = Sol.rates(); % evaluate V,T
		
      otherwise
	
	error('invalid time-stepping method')
	
    end
	
  end

  
  function [Sol,t,D,V,T] = timeStepMultiple(Sol,dt)
    % advances solution by time dt using multiple time steps
    
    switch Sol.timeStepMethod
      	
      case 'adaptiveRK'

	rtol = 1e-9; % relative tolerance
	atol = 1e3*eps; % absolute tolerance
	
	opt = odeset('RelTol',rtol,'AbsTol',atol);
	sol = ode45(@evalODE,[Sol.t Sol.t+dt],[Sol.D; Sol.Psi],opt,Sol);

	% update time
	Sol.t = Sol.t+dt;

	% evaluate fields
	t = sol.x;
	[Y,dYdt] = deval(t,sol);

	N = length(Sol.El.x);
	D = Y(1:N,:);
	Psi = Y(N+1:2*N,:);	
	V = dYdt(1:N,:);
	T = dYdt(N+1:2*N,:); % REALLY dPsi/dt
	
      otherwise
	
	error('invalid time-stepping method')
	
    end
	
  end
  
  
  function [t,D,V,T,Psi] = getSolution(Sol)
    t = Sol.t;
    D = Sol.D;
    V = Sol.V;
    T = Sol.T;
    Psi = Sol.Psi;
  end

  
  function dYdt = evalODE(t,Y,Sol)
  
    % returns V and DPsi in column vector Y for Matlab's ODE solver

    Sol.t = t;
    
    % extract slip and state
    N = length(Sol.El.x);
    Sol.D = Y(1:N);
    Sol.Psi = Y(N+1:2*N);
    
    % evaluate rates
    Sol = Sol.rates();
    
    % return rates as column vector
    dYdt = [Sol.V; Sol.DPsi];
    
  end
    
  
  end


  methods(Static)

  
  function [V,T,DPsi] = newtonRS(V,Psi,Tlock,Neff,eta,frictionParams)
    
    % solve for V in T=Tstr with Newton's method,
    % write as F(V)=T(V)-Tstr(V) and solve F(V)=0
    
    atol = 0; % absolute tolerance for convergence
    rtol = 1e-6; % relative tolerance for convergence
    
    nmax = 1000; % maximum number of iterations

    if Tlock<=0
      disp('Tlock<0')
      V = 0; T = Tlock; DPsi = 0;
      return
    end
    
    % initial guess
    if isnan(V), V=frictionParams.V0; end
    
    % velocity brackets, solution V is within [0 Vmax]
    Vmin = 0;
    Vmax = Tlock/eta;

    % ensure initial guess is within brackets
    if V<Vmin|V>Vmax, V = 0.5*(Vmin+Vmax); end
    
    for n=1:nmax
      
      % evaluate F = T-Tstr
      
      % stress T
      T = Tlock-eta*V;
      
      % standard log form of strength Tstr
      Tstr = Neff*(frictionParams.a*log(V/frictionParams.V0)+Psi);
      
      % regularized arcsinh form of strength Tstr
      %Omega = exp(Psi/frictionParams.a)/(2*frictionParams.V0);
      %Tstr = Neff*frictionParams.a*asinh(Omega*V);
      
      F = T-Tstr;

      % test for convergence on F
      if abs(F)<atol
	DPsi = solver.evolvePsi(V,Psi,frictionParams);
	return
      end
      
      % calculate derivative dFdV for Newton's method
      
      dTdV = -eta;
      dTstrdV = Neff*frictionParams.a/V; % log form
      %dTstrdV = Neff*frictionParams.a*Omega/sqrt(1+(Omega*V)^2); % arcsinh form
      
      dFdV = dTdV-dTstrdV; % derivative
      
      % Newton update
      dV = -F/dFdV;
      
      % check that update doesn't make V<=0, otherwise update to V/2
      % (routine assumes Tlock>=0 but could be generalized to any Tlock)
      %if V+dV<=0, dV=-V/2; end
      
      % ensure that update is within brackets
      if V+dV<Vmin|V+dV>Vmax, dV = 0.5*(Vmin+Vmax)-V; end
      
      % update V
      V = V+dV;
      
      % test for convergence on V
      if abs(dV)<atol+abs(V)*rtol
	DPsi = solver.evolvePsi(V,Psi,frictionParams);
	return
      end
      
    end

    disp('Newton did not converge; check results!')
    
    DPsi = solver.evolvePsi(V,Psi,frictionParams);
    
  end
  

  function DPsi = evolvePsi(V,Psi,frictionParams)

    switch frictionParams.evolutionLaw
      
      case 'slip' % slip law

	logV = log(V/frictionParams.V0);
	f = frictionParams.a*logV+Psi;
	fss = frictionParams.f0+(frictionParams.a-frictionParams.b)*logV;
	DPsi = V/frictionParams.dc*(fss-f); 
    
      case 'aging' % ageing law

	DPsi = frictionParams.b*frictionParams.V0/frictionParams.dc*(exp((frictionParams.f0-Psi)/frictionParams.b)-V/frictionParams.V0);
    
      otherwise
	
	error('invalid state evolution law')
	
    end
	
  end

  
  end

end
