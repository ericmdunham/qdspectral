% script to solve static crack problem:
% calculate stress change caused by slip
% and compare to exact antiplane shear solution

refine = 16; % mesh refinement factor
N = 512*refine; % number of grid points
L = 30; % domain length (m)
mu = 30e3; % shear modulus (MPa)
c = 3e3; % shear wave speed (m/s)

% additional parameters, not used here
Neff = NaN; % effective normal stress (MPa)
A = NaN; % amplitude of stress perturbation (MPa)
T0 = NaN; % initial shear stress (MPa)
W = NaN; % width of stress perturbation (m)
dTdt = NaN; % shear stressing rate (MPa/s)

% elasticity class
El = elasticity(N,L,mu,c,Neff,T0,A,W,dTdt);

% get x = distance along fault (m)
x = El.getGrid();

% specify slip
Lcrack = 2; % half-length of crack
Dmax = 1e-3; % maximum slip
D = Dmax*real(sqrt(1-(x/Lcrack).^2));

% calculate stress change
deltaT = El.slip2stress(D); % numerical
stressDrop = mu*Dmax/Lcrack/2; % stress drop (exact)
deltaTexact = zeros(N,1); % exact solution
deltaTexact = stressDrop*(abs(x)./sqrt(x.^2-Lcrack^2)-1); % singular stress outside
deltaTexact(abs(x)<Lcrack) = -stressDrop; % constant stress drop inside

% plot
subplot(2,1,1)
plot(x,D,'-o')
xlabel('distance (m)')
ylabel('slip (m)')
xlim([-2*Lcrack 2*Lcrack])

subplot(2,1,2)
plot(x,deltaT,'-o',x,deltaTexact)
xlabel('distance (m)')
ylabel('stress change (MPa)')
legend('numerical','exact')
ylim([-1.5*stressDrop 1.5*stressDrop])
xlim([-2*Lcrack 2*Lcrack])