classdef friction

% friction class stores slip-weakening and rate-and-state friction
% parameters

    properties

      frictionLaw % friction law ('slipweak' or 'ratestate')

      % slip-weakening parameters:
      fs % static friction
      fd % dynamic friction
      Dc % slip-weakening distance

      % rate-and-state parameters:
      evolutionLaw % state evolution law
      f0 % reference friction
      V0 % reference slip velocity
      dc % state evolution distance
      a % direct effect parameter
      b % evolution parameter
      widthVW % width of central velocity-weakening region
      aVS % direct effect parameter (outside VW region)
      bVS % evolution parameter (outside VW region)
      
    end

    methods
    
    
    function Fr = friction(frictionLaw,frictionParams)
      % class constructor
      Fr.frictionLaw = frictionLaw;
      switch Fr.frictionLaw
	case 'slipweak'
	  Fr.fs = frictionParams.fs;
	  Fr.fd = frictionParams.fd;
	  Fr.Dc = frictionParams.Dc;
	case 'ratestate'
	  Fr.evolutionLaw = frictionParams.evolutionLaw;
	  Fr.f0 = frictionParams.f0;
	  Fr.V0 = frictionParams.V0;
	  Fr.dc = frictionParams.dc;
	  Fr.a  = frictionParams.a;
	  Fr.b  = frictionParams.b;
	  Fr.widthVW = frictionParams.widthVW;
	  Fr.aVS  = frictionParams.aVS;
	  Fr.bVS  = frictionParams.bVS;
	otherwise
	  error('invalid friction law')
      end
    end


    function frictionParams = getParams(Fr,x)
      
      switch Fr.frictionLaw
	case 'slipweak'
	  frictionParams.fs = Fr.fs;
	  frictionParams.fd = Fr.fd;
	  frictionParams.Dc = Fr.Dc;
	case 'ratestate'
	  frictionParams.evolutionLaw = Fr.evolutionLaw;
	  frictionParams.f0 = Fr.f0;
	  frictionParams.V0 = Fr.V0;
	  frictionParams.dc = Fr.dc;
	  if abs(x)<=Fr.widthVW/2
	    frictionParams.a  = Fr.a;
	    frictionParams.b  = Fr.b;
	  else
	    frictionParams.a = Fr.aVS;
	    frictionParams.b = Fr.bVS;
	  end
	otherwise
	  error('invalid friction law')
      end
      
    end
    
    
    function f = frictionCoeff(Fr,x,D)
      % evaluates friction coefficient f at position x on fault
      % given slip D
      N = length(x); f = zeros(N,1);
      for i=1:N
	f(i) = Fr.fs-(Fr.fs-Fr.fd)*min(D(i),Fr.Dc)/Fr.Dc;
      end
    end
    
    
    end

end